package com.mj;

public class Stu<T> {
    private T t;
    public Stu(T t){
        this.t = t;
    }
    public void info(){
        System.out.println(t.getClass().getName());
    }
}

package com.mj;

public class User {
    public static void main(String[] args) {
        Stu<Integer> stu1 = new Stu<Integer>(18);
        stu1.info();
        Stu<String> str2 = new Stu<String>("mike");
        str2.info();
    }
}

package com.mj;

import com.mj.dao.StudentDao;
import com.mj.entity.Book;
import com.mj.entity.Student;
import com.mj.mapper.BookMapper;
import com.mj.mapper.StudentMapper;
import com.mj.mybatis.MyBatisHelper;
import org.apache.ibatis.session.SqlSession;
import org.junit.Test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Demo {
    @Test
    public void qua(){
        StudentDao suu = new StudentDao();
        List<Map<String,Object>> mss = suu.querySome();
        for (Map<String,Object> nk: mss){
            System.out.println(nk.get("name"));
        }
    }
    @Test
    public void qus() {
        StudentDao sud = new StudentDao();
        Map<String, Object> ms = sud.queryById(13);
        System.out.println(ms.get("name"));
        System.out.println(ms.get("score"));
    }

    @Test
    public void qn() {
        StudentDao std = new StudentDao();
        System.out.println(std.count());
        List<Student> sl = std.queryByName("%飞%");
        for (Student sy : sl) {
            System.out.println(sy.getName());
        }
    }
    @Test
    public void sa() {
        MyBatisHelper mh = new MyBatisHelper();
        SqlSession s = mh.getSession();
        BookMapper bmp = s.getMapper(BookMapper.class);
       /* Map<String,Object>map = new HashMap<String, Object>();
        map.put("name","双城记");
        map.put("price",99);
        map.put("date","2017-8-28");
        bmp.save(map);*/
        //另一种方法
        bmp.add("老人与海", 200, "2017-8-31");
        s.commit();
    }

    @Test
    public void fina() {
        StudentDao sd = new StudentDao();
        sd.deleteById(52);
        List<Student> fs = sd.queryAll();
        for (Student fi : fs) {
            System.out.println(fi.getName());
        }
    }

    @Test
    public void ttt() {
        BookMapper bmd = new MyBatisHelper<BookMapper>(BookMapper.class).getMapper();
        List<Book> bbk = bmd.query();
        System.out.println(bbk.size());
    }

    @Test
    public void sss() {
        //StudentMapper stm = new MyBatisHelper<StudentMapper>(StudentMapper.class).getMapper();
        MyBatisHelper<StudentMapper> mh = new MyBatisHelper<StudentMapper>();
        StudentMapper sma = mh.getMapper(StudentMapper.class);
        sma.deleteById(31);
        List<Student> stt = mh.getMapper(StudentMapper.class).queryAll();
        for (Student st : stt) {
            System.out.println(st.getName());
        }
        mh.close();
        //System.out.println(.size());
    }

    @Test
    public void show() {
        SqlSession s = new MyBatisHelper<BookMapper>().getSession();
        BookMapper bm = s.getMapper(BookMapper.class);
        //数据插入
        /*Book book = new Book();
        book.setName("傲慢与偏见");
        book.setPrice(150);
        try {
            book.setDate(new SimpleDateFormat("yyyy-MM-dd").parse("2017-8-31"));
        } catch (ParseException e) {
            e.printStackTrace();
        }*/
        Book bb = bm.queryById(3);
        System.out.println(bb.getName());
        //bm.insert(book);
        //数据查询
        List<Book> bk = bm.query();
        for (Book b : bk) {
            System.out.println(b.getName());
        }
        s.commit();
    }
}

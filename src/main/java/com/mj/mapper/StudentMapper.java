package com.mj.mapper;

import com.mj.entity.Student;
import org.apache.ibatis.annotations.Select;

import java.util.List;
import java.util.Map;

public interface StudentMapper {
 public List<Student> queryAll();
 public int deleteById(int id);
 @Select("select * from student where name like #{name}")
 public List<Student> queryByName(String n);
 @Select("select count(*) from student")
 public int count();
 @Select("select name,score from student where id = #{id}")
 public Map<String,Object> queryById(int id);
 @Select("select name,score from student where 1=1")
 public List<Map<String,Object>>querySome();
}

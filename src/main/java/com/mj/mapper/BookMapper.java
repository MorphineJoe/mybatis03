package com.mj.mapper;

import com.mj.entity.Book;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;
import java.util.Map;

public interface BookMapper {
    @Select("select * from book")
    public List<Book> query();
    @Insert("insert into book values(null,#{name},#{price},#{date})")
    public int insert(Book book);
    public Book queryById(int id);
    @Insert("insert into book values(null,#{name},#{price},#{date})")
    public int save(Map<String,Object> map);
    @Insert("insert into book values(null,#{n},#{p},#{d})")
    public int add(@Param("n")String name,@Param("p")double price,@Param("d") String date);
    @Insert("insert into book values(null,${0},${1},${2})")
    public int bookadd(String name,double price,String date);
    @Select("select * from book where name like #{name}")
    public List<Book> queryByName();
}

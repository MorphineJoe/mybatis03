package com.mj.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data @AllArgsConstructor @NoArgsConstructor
public class Student {
    private int id;
    private String name;
    private int age;
    private String dept;
    private String score;
    public Student(String name,int age,String dept,String score){
        this.name = name;
        this.age = age;
        this.dept = dept;
        this.score = score;
    }
}

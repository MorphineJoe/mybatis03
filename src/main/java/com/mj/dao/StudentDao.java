package com.mj.dao;

import com.mj.entity.Student;
import com.mj.mapper.StudentMapper;
import com.mj.mybatis.MyBatisHelper;
import org.apache.ibatis.session.SqlSession;

import java.util.List;
import java.util.Map;

public class StudentDao implements StudentMapper{
    private SqlSession session = new MyBatisHelper().getSession();
    private StudentMapper sdao;
    public StudentDao(){
        this.sdao = session.getMapper(StudentMapper.class);
    }
    public List<Student> queryAll() {
        return this.sdao.queryAll();
    }

    public int deleteById(int id) {
        int num;
        num = this.sdao.deleteById(id);
        session.commit();
        return num;
    }

    public List<Student> queryByName(String n) {
        return this.sdao.queryByName(n);
    }

    public int count() {
        return this.sdao.count();
    }

    public Map<String, Object> queryById(int id) {
        return this.sdao.queryById(id);
    }

    public List<Map<String, Object>> querySome() {
        return this.sdao.querySome();
    }
}
